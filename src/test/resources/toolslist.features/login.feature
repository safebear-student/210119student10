Feature: Login
  In order to access my acc
  As a user
  I want to know if login is successful

  Rules:
  * User must be informed success
  * User must be informed if failed

  Glossary:
  * User

  @HighRisk
  @Regression
  Scenario Outline: Navigate and login to the app
    Given I navigate to the login page
    When I enter login details for a'<userType>'
    Then I can see the following message'<validationMessage>'
    Examples:
      | userType    | validationMessage                 |
      | invalidUser | Username or Password is incorrect |
      | validUser   | Login Successful                  |
