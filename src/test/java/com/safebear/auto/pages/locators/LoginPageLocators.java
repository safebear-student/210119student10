package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LoginPageLocators {

    private By usernameFieldlocator = By.id("username");
    private By passwordlocator = By.id("password");
    private By loginbuttonlocator = By.id("enter");

    private By FailedloginMessage = By.xpath(".//p[@id='rejectLogin']/b");

}

