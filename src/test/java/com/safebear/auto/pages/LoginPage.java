package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import com.safebear.auto.utils.Properties;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {
    LoginPageLocators locators = new LoginPageLocators();

    @NonNull
    WebDriver driver;
    String expectedPageTitle = "Login Page";


    public String getPageTitle() {
        return driver.getTitle();

    }

    public String getExpectedPagetitle() {
        return expectedPageTitle;
    }

    public void enterUsername(String username) {

        Properties.waitForElement(locators.getUsernameFieldlocator(),driver).sendKeys(username);

    }

    public void enterPassword (String password) {
        driver.findElement(locators.getPasswordlocator()).sendKeys(password);
    }

    public void clickloginButton() {
        driver.findElement(locators.getLoginbuttonlocator()).click();
    }

    public String checkForFailedloginWarning() {
        return driver.findElement(locators.getFailedloginMessage()).getText();
    }

     public void login(String username, String password){

    enterUsername(username);
    enterPassword(password);
    clickloginButton();
        }
    }


