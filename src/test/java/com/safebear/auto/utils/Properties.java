package com.safebear.auto.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;

public class Properties {

    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080");
    private static final String BROWSER = System.getProperty("BROWSER", "chrome");

    public static String getUrl() {
        return URL;
    }

    public static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver",
                "src/test/resources/drivers/chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,768");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver();

            case "firefox":
                return new FirefoxDriver();

            case "headless":
                options.addArguments("headless", "disable-gpu");
                return new ChromeDriver(options);

            default:
                return new ChromeDriver();

        }
    }

    public static String generateScreenShotFilename(String testname) {
        //create filename
        return testname + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".png";
    }

    public static void capturescreenshot(WebDriver driver, String filename) {

        //take screenshot
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        //make sure that the 'screenshots' directory exists
        File file = new File("target/screenshots");

        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory");

            }
        }
        //Copy file to filename and location we set before
        //
        try {
            copy(scrFile, new File("target/screenshots/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static WebElement waitForElement(By locator, WebDriver driver) {
        // set the max length of time it will wait
        WebDriverWait wait = new WebDriverWait(driver, 5);

        WebElement element;

        //try and find the element

        try {
            element = driver.findElement(locator);
        } catch (TimeoutException e) {

            //if the element isn't immediately found use an explicit wait...
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            element = driver.findElement(locator);
        } catch (Exception e) {
            // if the element still isn't there, print a stack trace and take a screenshot
            System.out.println(e.toString());
            capturescreenshot(driver, generateScreenShotFilename("WebElementFailure"));
            throw (e);
        }
        return element;
    }
}