package com.safebear.auto.nonBddtests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Properties;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class BaseTest {
    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;

    @BeforeTest
    public void setUp() {
        driver = Properties.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }

    @AfterTest
    public void tearDown() {

        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "1000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }



}

