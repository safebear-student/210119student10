package com.safebear.auto.nonBddtests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.utils.Properties;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {


    @Test
    public void validLogin() {
        // Step 1 ACTION: Open our web application in the browser
        driver.get(Properties.getUrl());
        // Step 1 EXPECTED: check we're on the login page
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPagetitle(), "Tools Page");
        // 2. Action login in as a valid user
        loginPage.login("tester", "letmein");
        // 2. Expected Check Im on Tools Page
        Assert.assertTrue(toolsPage.checkForSuccessfulLoginMessage().contains("Success"));
    }

    @Test
    public void invalidLogin() {
        // Step 1 ACTION: Open our web application in the browser
        driver.get(Properties.getUrl());
        // Step 1 EXPECTED: check we're on the login page
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPagetitle(), "Tools Page");
        // 2. Action login in as a valid user
        loginPage.login("hacker", "letmein");
        // 2. Expected Check Im on Login Page
        Assert.assertTrue(loginPage.checkForFailedloginWarning().contains("incorrect"));
    }
}