package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Properties;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {

    WebDriver driver;

    LoginPage loginPage;
    ToolsPage toolsPage;

    @Before
    public void setUp() {
        driver = Properties.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }

    @After
    public void tearDown() {

        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }


    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        // Step 1 ACTION: Open our web application in the browser
        driver.get(Properties.getUrl());
        // check we're on teh login page
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPagetitle(), "We're not on the loginPage or its title has changed");
    }

    @When("^I enter login details for a'(.+)'$")
    public void i_enter_login_details_for_a_USER(String userType) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        switch (userType) {
            case "validUser":
                loginPage.enterUsername("tester");
                loginPage.enterPassword("letmein");
                loginPage.clickloginButton();
                break;
            case "invalidUser":
                loginPage.enterUsername("attacker");
                loginPage.enterPassword("dontletmein");
                loginPage.clickloginButton();
              Properties.capturescreenshot(driver,Properties.generateScreenShotFilename("Invalid user login"));
                break;
            default:
               //Properties.capturescreenshot(driver,Properties.generateScreenShotFilename());

                Assert.fail("The test data is wrong - the only values that can be accepted are 'validUser' or 'invalidUser'");
                break;

        }
    }

    @Then("^I can see the following message'(.+)'$")
    public void i_can_see_the_following_message(String validationMessage) throws Throwable {
        switch (validationMessage) {
            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.checkForFailedloginWarning().contains(validationMessage));
                break;
            case "Login Successful":
                Assert.assertTrue(toolsPage.checkForSuccessfulLoginMessage().contains(validationMessage));
                break;
            default:
                Assert.fail("the test data is wrong");
                break;
        }
    }

}


