package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {

    @Test
    public void testEmployee() {

//this is where we create our objects
        Employee hannah = new Employee();
        Employee bob = new Employee();
        SalesEmployee victoria = new SalesEmployee();

//this is where employ hannah and fire bob
        hannah.employ();
        bob.fire();
        Accounts.calculateTurnover();
        Accounts.countEmployees();

        //This where we employ victoria and give her a bmw
        victoria.employ();
        victoria.changeCar("bmw");

//Lets print their state to screen
        System.out.println("Hannah employment state: " + hannah.isEmployed());
        System.out.println("Bob employment state: " + bob.isEmployed());
        System.out.println("Victoria employment state: " + victoria.isEmployed());
        System.out.println("Victoria's car:" + victoria.car);
        System.out.println("Turnover:" + Accounts.turnOver);
        System.out.println("No of Employees:" + Accounts.numberOfEmployees);
    }

}